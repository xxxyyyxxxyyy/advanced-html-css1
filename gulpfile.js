import gulp from "gulp";
const { src, dest, series } = gulp;

import csso from "gulp-csso";
import autoprefixer from "gulp-autoprefixer";

const htmlTask = () => src("./src/index.html").pipe(dest("./dist"));

const stylesTask = () => 
	src("./src/css/*.css")
	.pipe(autoprefixer())
	.pipe(csso())
	.pipe(dest("./dist"));

export const html = htmlTask;
export const styles = stylesTask;
export const dev = series(htmlTask, stylesTask);